package parser

import (
	"net/http"
	"gitlab.com/partyzan65/gotask/log"
	"fmt"
	"encoding/json"
)

type Handler struct {
	ch     chan UrlInfo
	Logger *log.Logger
}

func (h Handler) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	var resp []byte
	statusCode := http.StatusOK

	urls := Urls{}
	err := urls.SetFromRequest(request)
	if err != nil {
		statusCode = http.StatusBadRequest
		h.Logger.WriteError(err)
	} else {
		data := h.LoadAll(urls)
		resp, err = json.Marshal(data)
		if err != nil {
			statusCode = http.StatusInternalServerError
			h.Logger.WriteError(err)
		}
	}

	response.Header().Set("content-type", "application/json; charset=utf-8")
	response.WriteHeader(statusCode)
	fmt.Fprint(response, string(resp))
}

func (h *Handler) LoadAll(urls Urls) []UrlInfo {
	var response []UrlInfo
	total := len(urls)
	count := 0
	h.ch = make(chan UrlInfo, total)

	for _, url := range urls {
		go h.Load(url)
	}

	for urlInfo := range h.ch {
		response = append(response, urlInfo)
		count++
		if count == total {
			close(h.ch)
			break
		}
	}

	return response
}

func (h *Handler) Load(url string) {
	urlInfo := UrlInfo{Url: url}

	resp, err := http.Get(url)
	if err != nil {
		h.ch <- urlInfo
		return
	}

	urlInfo.Meta = Meta{Status: resp.StatusCode}
	urlInfo.SetHeaders(resp.Header)
	urlInfo.SetElements(resp.Body)
	h.ch <- urlInfo
}

func NewHandler(logger *log.Logger) *Handler {
	return &Handler{Logger: logger}
}
