package parser

import (
	"testing"
	"net/http"
	flog "gitlab.com/partyzan65/gotask/log"
	"strings"
	"encoding/json"
	"io/ioutil"
	"net/http/httptest"
	"fmt"
)

func TestHandler_ServeHTTP(t *testing.T) {
	t.Run("Test status", func(t *testing.T) {
		logger, _ := flog.NewLogger("", true)
		handler := NewHandler(logger)
		ts := httptest.NewServer(handler)

		data, err := json.Marshal([]string{"https://google.com", "https://yandex.ru", "https://yandex.ru/qwerty"})
		resp, err := http.Post(ts.URL, "application/json", strings.NewReader(string(data)))
		if err != nil {
			t.Fatal(err)
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatal(err)
		}
		defer resp.Body.Close()

		var urls []UrlInfo
		err = json.Unmarshal(body, &urls)
		if err != nil {
			t.Fatal(err)
		}

		if len(urls) != 3 {
			t.Error("No lenght, expected 3")
		}

		for _, urlInfo := range urls {
			switch urlInfo.Url {
			case "https://google.com", "https://yandex.ru":
				if urlInfo.Meta.Status != http.StatusOK {
					t.Error("Invalid http status, expected 200, host: " + urlInfo.Url)
				}
			case "https://yandex.ru/qwerty":
				if urlInfo.Meta.Status != http.StatusNotFound {
					t.Error("Invalid http status, expected 404, host: " + urlInfo.Url)
				}
			}
		}
	})

	html := "123<html lang=\"en\">X<head><meta charset=\"UTF-8\"><title></title></head>T<body><b></b><b></b></body></html>qwerty"
	expected := map[string]uint{
		"html": 1,
		"head": 1,
		"title": 1,
		"meta": 1,
		"body": 1,
		"b": 2,
	}

	t.Run("Test elements", func(t *testing.T) {
		// handler
		logger, _ := flog.NewLogger("", true)
		handler := NewHandler(logger)
		ts1 := httptest.NewServer(handler)

		// test page

		ts2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("content-type", "application/json; charset=utf-8")
			w.Header().Set("X-test", "test-header")
			fmt.Fprint(w, html)
		}))

		data, err := json.Marshal([]string{ts2.URL})
		resp, err := http.Post(ts1.URL, "application/json", strings.NewReader(string(data)))
		if err != nil {
			t.Fatal(err)
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatal(err)
		}
		defer resp.Body.Close()

		var urls []UrlInfo
		err = json.Unmarshal(body, &urls)
		if err != nil {
			t.Fatal(err)
		}

		if len(urls) != 1 {
			t.Error("No lenght, expected 1")
		}

		for _, urlInfo := range urls {
			if urlInfo.Meta.Status != http.StatusOK {
				t.Error("Invalid http status, expected 200, host: " + urlInfo.Url)
			}
			if len(urlInfo.Elements) != 6 {
				t.Error("Invalid elements lenght, expected 6")
			}

			for _, elem := range urlInfo.Elements {
				if count, ok := expected[elem.TagName]; !ok {
					t.Error("Tag " + elem.TagName + " is not found")
				} else {
					if count != elem.Count {
						t.Error("Number of tags does not match, tags: " + string(elem.Count) + ", expected: " + string(count))
					}
				}
			}

			if header, ok := urlInfo.Meta.Headers[0]["X-Test"]; !ok || header != "test-header" {
				t.Error("Invalid header")
			}
		}
	})
}


