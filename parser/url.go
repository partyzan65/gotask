package parser

import (
	"io"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"golang.org/x/net/html"
)

// url list
type Urls []string

// parsing urls from request.Body
func (u *Urls) SetFromRequest(request *http.Request) error {
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return err
	}
	defer request.Body.Close()

	err = json.Unmarshal(body, &u)
	if err != nil {
		return err
	}

	u.unique()
	return nil
}

// delete duplicate URL
func (u *Urls) unique() {
	arr := *u
	a := make([]string, 0, len(arr))
	b := make(map[string]bool)

	for _, val := range arr {
		if _, ok := b[val]; !ok {
			b[val] = true
			a = append(a, val)
		}
	}

	*u = a
}

type Meta struct {
	Status int `json:"status"`
	Headers []Header `json:"headers"`
}

type Header map[string]string

type Element struct {
	TagName string `json:"tag-name"`
	Count uint `json:"count"`
}

// structure for json-response
type UrlInfo struct {
	Url string `json:"url"`
	Meta Meta `json:"meta"`
	Elements []Element `json:"elements"`
	counter map[string]uint
}

// parsing amd counting of elements
func (ui *UrlInfo) SetElements(body io.Reader) {
	tok := html.NewTokenizer(body)
	ui.counter = make(map[string]uint)

	for {
	repeat:
		switch tok.Next() {
		case html.ErrorToken:
			goto end
		case html.StartTagToken, html.SelfClosingTagToken:
			name, _ := tok.TagName()
			if name == nil {
				goto repeat
			}

			ui.AddTag(name)
		}
	}

end:
	for tagName, count := range ui.counter {
		ui.Elements = append(ui.Elements, Element{TagName: tagName, Count: count})
	}
}

// counting tags
func (ui *UrlInfo) AddTag(name []byte) {
	tagName := string(name)
	ui.counter[tagName]++
}

// set headers
func (ui *UrlInfo) SetHeaders(header http.Header) {
	m := maxLen(header)
	headers := make([]Header, m)
	var item Header
	for key, items := range header {
		n := len(items)
		for i := 0; i < n; i++ {
			if item = headers[i]; item == nil {
				item = make(Header)
			}
			item[key] = items[i]
			headers[i] = item
		}
	}
	ui.Meta.Headers = headers
}

func maxLen(header http.Header) int {
	var n, max int
	for _, items := range header {
		n = len(items)
		if n > max {
			max = n
		}
	}

	return max
}
