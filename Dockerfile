FROM ubuntu
RUN apt-get update
RUN apt-get install -y ca-certificates
WORKDIR /root/
COPY gotask .
COPY config.json .
CMD ["./gotask"]
EXPOSE 8090