package main

import (
	"log"
	"net/http"
	"runtime"

	"gitlab.com/partyzan65/gotask/parser"
	"gitlab.com/partyzan65/gotask/conf"
	flog "gitlab.com/partyzan65/gotask/log"
)

var config, errConf = conf.NewConfig("./config.json")
var logger, errLog = flog.NewLogger("./gotask.log", !config.Get("log").Bool())

func main() {
	if maxProcs := config.Get("max_procs").Int(); maxProcs > 0 {
		runtime.GOMAXPROCS(maxProcs)
	}

	handleError(errLog)
	handleError(errConf)

	host := config.Get("host").String()
	port := config.Get("port").String()
	handler := parser.NewHandler(logger)

	log.Println("Listen on " + host + ":" + port)
	logger.Write("Started")
	log.Fatal(http.ListenAndServe(host + ":" + port, handler))
}

func handleError(err error)  {
	if err != nil {
		panic(err)
	}
}
