deps:
	glide install
build:
	GOOS=linux go build -o gotask ./server/main.go
	docker build --no-cache -t partydev/gotask .
run:
	docker run --name gotask -p "8090:8090" -d partydev/gotask
clean:
	rm ./gotask
