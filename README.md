# Тестовая задача для Golang разработчика


## Задача

- Форкнуть репозиторий
- Написать сервис на Golang, который принимает массив URL-ов в теле, 
для данных URL он должен загрузить инф. о кол-во тегов на странице, код ответа, 
и все заголовки, пример ответа:

``` json
[
  {
    "url": "http://www.example.com/",
    "meta": {
      "status": 200,
      
      "headers": [
        {
          "content-type": "text\/html",
          "server": "nginx",
          "content-length": 605,
          "connection": "close",
          // ...
        }
      ]
    },
    "elemets": [
      {
        "tag-name": "html",
        "count": 1
      },
      {
        "tag-name": "head",
        "count": 1
      },
      // ...
    ]
  },
  // ...
]
```
- Сервис необходимо завернуть в Docker

## Решение

```bash
git clone https://gitlab.com/partyzan65/gotask
cd ./gotask
// если есть glide
glide install 
// или
go get golang.org/x/net/html
make build && make run
```

### Пример запроса

```bash
curl -X POST -d "[\"https://yandex.ru\", \"https://gooooooogle.com\"]" http://localhost:8090/
```

### Пример ответа

```json
[
    {
        "url": "https://yandex.ru",
        "meta": {
            "status": 200,
            "headers": [
               {
                   "Cache-Control": "no-cache,no-store,max-age=0,must-revalidate",
                   "Content-Security-Policy": "connect-src 'self' wss://webasr.yandex.net https://mc.webvisor.com https://mc.webvisor.org wss://push.yandex.ru wss://portal-xiva.yandex.net https://yastatic.net https://home.yastatic.net https://yandex.ru https://*.yandex.ru static.yandex.sx brotli.yastatic.net et.yastatic.net portal-xiva.yandex.net yastatic.net home.yastatic.net yandex.ru *.yandex.ru *.yandex.net yandex.st; default-src 'self' blob: wss://portal-xiva.yandex.net yastatic.net portal-xiva.yandex.net; font-src 'self' https://yastatic.net static.yandex.sx brotli.yastatic.net et.yastatic.net yastatic.net; frame-src 'self' yabrowser: data: https://ok.ru https://www.youtube.com https://player.video.yandex.net https://yastatic.net https://yandex.ru https://*.yandex.ru wfarm.yandex.net yastatic.net yandex.ru *.yandex.ru awaps.yandex.net *.cdn.yandex.net; img-src 'self' data: https://yastatic.net https://home.yastatic.net https://*.yandex.ru https://*.yandex.net https://*.tns-counter.ru www.tns-counter.ru awaps.yandex.net *.yastatic.net gdeua.hit.gemius.pl pa.tns-ua.com mc.yandex.com mc.webvisor.com mc.webvisor.org static.yandex.sx brotli.yastatic.net et.yastatic.net yastatic.net home.yastatic.net yandex.ru *.yandex.ru *.yandex.net *.tns-counter.ru *.gemius.pl yandex.st; media-src 'self' blob: data: *.storage.yandex.net yastatic.net kiks.yandex.ru strm.yandex.ru; object-src 'self' *.yandex.net music.yandex.ru strm.yandex.ru yastatic.net kiks.yandex.ru awaps.yandex.net storage.mds.yandex.net; report-uri https://csp.yandex.net/csp?from=big.ru&showid=1510667348.71913.22890.28256&h=i19&yandexuid=6568628701510667348; script-src 'self' 'unsafe-inline' 'unsafe-eval' blob: https://suburban-widget.rasp.yandex.ru https://suburban-widget.rasp.yandex.net https://music.yandex.ru https://mc.yandex.fr https://mc.webvisor.com https://yandex.fr https://mc.webvisor.org https://yastatic.net https://home.yastatic.net https://mc.yandex.ru https://pass.yandex.ru www.youtube.com *.ytimg.com an.yandex.ru api-maps.yandex.ru static.yandex.sx webasr.yandex.net brotli.yastatic.net et.yastatic.net yastatic.net home.yastatic.net yandex.ru www.yandex.ru mc.yandex.ru suggest.yandex.ru clck.yandex.ru awaps.yandex.net; style-src 'self' 'unsafe-inline' https://yastatic.net https://home.yastatic.net static.yandex.sx brotli.yastatic.net et.yastatic.net yastatic.net home.yastatic.net;",
                   "Content-Type": "text/html; charset=UTF-8",
                   "Date": "Tue, 14 Nov 2017 13:49:08 GMT",
                   "Expires": "Tue, 14 Nov 2017 13:49:09 GMT",
                   // ...
               },
               // ...
           ]
        },
        "elements": [
            {
                "tag-name": "img",
                "count": 4
            },
            {
                "tag-name": "noscript",
                "count": 4
            },
            // ...
        ]
    },
    {
        "url": "https://gooooooogle.com",
        "meta": {
            "status": 0,
            "headers": null
        },
        "elements": null
    },
    // ...
]
```